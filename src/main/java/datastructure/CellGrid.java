package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
	
	private int rows;
	private int columns;
	private CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
    	this.rows = rows;
    	this.columns = columns;
    	this.grid = new CellState[rows][columns];
    	for (int i=0; i<rows; i++) {
    		for (int j=0; j<columns; j++) {
    			grid[i][j] = initialState;
    		}
    	}	
	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
    	CellGrid copiedGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
    	for (int i=0; i<rows; i++) {
    		for (int j=0; j<columns; j++) {
    			copiedGrid.grid[i][j] = this.grid[i][j];
    		}
    	}
        return copiedGrid;
    }
    
}
